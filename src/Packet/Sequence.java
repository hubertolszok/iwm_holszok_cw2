package Packet;

public abstract class Sequence <T> extends Packet{
	protected int channelNr;
	protected String unit;
	protected double resolution;
	protected T buffer[];
	
	public Sequence(String unit, int channelNr, double resolution, T[] buffer,String device, String description, long date){
		super(device,description,date);
		this.channelNr = channelNr;
		this.unit = unit;
		this.resolution = resolution;
		this.buffer = buffer;
		
	}
	public String toString(){
		return "ChannelNr: " + channelNr + "\nUnit: " + unit + "\nResolution: " + resolution + "Buffer: " + buffer[0];
	}
	
	
	
}
