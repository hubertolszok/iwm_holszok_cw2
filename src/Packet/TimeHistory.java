package Packet;
import java.io.Serializable;

public class TimeHistory<T> extends Sequence {
	public double sensitivity;

	public TimeHistory(double sensitivity, String unit, int channelNr, double resolution, T[] buffer ,String device, String description, long date)
	{
		super(unit,channelNr,resolution,buffer,device,description,date);
		this.sensitivity=sensitivity;
	}
	
	//metoda toString, kt�ra zwraca parametry klasy
	public String toString()
	{		
	return "\nSensitivity: " + sensitivity + "\nUnit: " + unit + "\nChannelNr: " + channelNr + "\nResolution: " + resolution + "\nBuffer: " + buffer[0] + "\nDevice: " + device + "\nDescription: " + description + "\nDate: " + date;
	}

}
