package Packet;

public class Alarm extends Packet {
	public int channelNr;
	public int threshold;
	public int direction;

	public Alarm(int channelNr, int threshold, int direction, String device, String description, long date) {
		super(device,description,date);
		this.channelNr = channelNr;
		this.threshold = threshold; // 0-dowolny,1-gora,-1-dol
		this.direction = direction;
	}


	public String toString() {
		return "\nChannelNr: " + channelNr + "\nThreshold: " + threshold + "\ndirection: " + direction + "\nDevice: " + device + "\nDescription: " + description + "\nDate: " + date;
	}

}
