package Packet;

import java.io.Serializable;

public abstract class Packet implements Serializable{
	protected String device;
	protected String description;
	protected long date;
	
	public Packet(String device, String description, long date){
		this.device = device;
		this.description = description;
		this.date = date;
		
	}
	public String toString()
	{
		return "Device: " + device + "\nDescription: " + description + "\nDate: " + date;
	}
	
	public String toPath() {
		return "C:/Studia/InformatykaWMechatronice/Lab2" + device + "_" + description + "_" + date;
	}
	
}
