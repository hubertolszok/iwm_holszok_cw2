package lab2;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

import Packet.Packet;

public class UdpServer {
	public static void main(String[] args) throws ClassNotFoundException, ClassCastException {

		System.out.println("Start Serwera");
		DatagramSocket aSocket = null;
		try {
			aSocket = new DatagramSocket(1234);
			byte[] buffer = new byte[1024];
			while (true) {
				DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
				System.out.println("Waiting for request...");
				aSocket.receive(reply);

				Request read = (Request) Tools.deserialize(reply.getData());
				System.out.println("Received: \n");
				System.out.println(read.getObject().toString());

				String feedback = "";
				File myObj = new File("C:/Studia/InformatykaWMechatronice/Lab2");
				File[] files = null;

				switch (read.getType()) {
				case SAVE:

					Packet receivedObject = (Packet) read.getObject();
					Files.write(new File(receivedObject.toPath()).toPath(), Tools.serialize(receivedObject));
					feedback = new String("Serwer zapisal obiekt");

					break;

				case LIST:

					files = myObj.listFiles();
					feedback = "\n Files: ";
					if (files != null && files.length > 0) {
						for (File file : files) {
							if (file.isFile() && !file.getName().startsWith(".")) {
								feedback = feedback + "\n-" + file.getName();
							}
						}
					}

					break;

				case SEARCH:
					files = myObj.listFiles((dir1, name) -> name.contains("urzadzenie"));
					feedback = "\n Files: ";
					if (files != null && files.length > 0) {
						for (File file : files) {
							feedback = feedback + "\n-" + file.getName();
						}
					} else {
						feedback = "No Files found:";
					}
					break;

				default:

					feedback = "SAVE/SEARCH/LIST";

					break;

				}

				DatagramPacket reply1 = new DatagramPacket(feedback.getBytes(), feedback.length(), reply.getAddress(),
						reply.getPort());
				aSocket.send(reply1);
			}
		} catch (SocketException ex) {
			Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			aSocket.close();
		}

	}
}
