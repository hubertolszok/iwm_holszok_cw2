package lab2;

import java.io.Serializable;
import Packet.Packet;

public class Request extends Packet implements Serializable {

	public enum Type {
		SAVE, LIST, SEARCH
	}

	private Type type;
	private String value;
	private Object object;

	Request() {
		super("", "", 0);
		value = "";
		type = Type.SAVE;
		object = null;
	}

	Request(String device, String description, long date, Type type, String value, Object object) {
		super(device, description, date);
		this.type = type;
		this.value = value;
		this.object = object;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String toString() {

		String val;
		val = super.toString() + "\nType: " + type + "\nValue " + value;
		return val;
	}

}
