package lab2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import Packet.TimeHistory;

public class UDPClient {
	public static void main(String[] args) throws ClassNotFoundException, ClassCastException {

		DatagramSocket aSocket = null;
		int serverPort = 1234;
		System.out.println("START");

		long date = System.currentTimeMillis() / 1000;
		TimeHistory Packet = new TimeHistory(100, "mm", 2, 480, new Integer[] { 1, 2, 3 }, "device", "dev1", date);

		Request object = new Request();
		object.setType(Request.Type.SEARCH);
		object.setValue("7");
		object.setObject(Packet);

		try {
			byte[] buffer = new byte[1024];
			byte[] data = Tools.serialize(object);
			InetAddress address = InetAddress.getLocalHost();
			aSocket = new DatagramSocket();

			DatagramPacket request = new DatagramPacket(data, data.length, address, serverPort);
			aSocket.send(request);
			System.out.println("wysylanie");

			DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
			aSocket.receive(reply);

			String odp = new String(reply.getData(), 0, reply.getLength());
			System.out.println(odp);

		} catch (SocketException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			aSocket.close();
		}
	}
}